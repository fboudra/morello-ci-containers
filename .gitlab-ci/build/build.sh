#!/bin/sh

set -e

buildah login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

set -x

buildah pull ubuntu:bionic || true

date=$(date +%Y%m%d-%H%M%S)
for module in ${CONTAINER}; do
  tag1="${CI_REGISTRY_IMAGE}/morello-${module}:${CI_COMMIT_REF_NAME}.${CI_PIPELINE_ID}"
  tag2="${CI_REGISTRY_IMAGE}/morello-${module}:${date}"
  if [ ${CI_COMMIT_REF_NAME} = "master" ]; then
    tag3="${CI_REGISTRY_IMAGE}/morello-${module}:latest"
  else
    tag3="${CI_REGISTRY_IMAGE}/morello-${module}:${CI_COMMIT_REF_NAME}.latest"
  fi
  buildah bud --tag "${tag1}" --tag "${tag2}" --tag "${tag3}" --squash ${module}/
  # Skip model publishing. It isn't allowed to redistribute the model.
  if [ ${module} = "model" ]; then
    continue
  fi
  buildah push ${tag1}
  buildah push ${tag2}
  buildah push ${tag3}
done

buildah images
