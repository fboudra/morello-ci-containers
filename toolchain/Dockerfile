FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive
# cmake and make are installed from toolchain-src/prebuilts/build-tools/
ENV PKG_DEPS="\
    bison \
    ca-certificates \
    curl \
    git \
    gnupg \
    less \
    libtool \
    python3 \
    python3-distutils \
    rsync \
    sudo \
"

# Can be overriden at build time
ARG BUILDSLAVE_PASSWORD=buildslave

RUN set -e ;\
    echo 'dash dash/sh boolean false' | debconf-set-selections ;\
    apt update -q=2 ;\
    apt full-upgrade -q=2 --yes ;\
    apt install -q=2 --yes --no-install-recommends ${PKG_DEPS} ;\
    # Set default shell to bash
    dpkg-reconfigure -p critical dash ;\
    # Set Python 3 as default
    update-alternatives --install /usr/bin/python python /usr/bin/python3 50 ;\
    # Install repo
    curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/bin/repo ;\
    chmod a+x /usr/bin/repo ;\
    # Setup buildslave user
    useradd -m -s /bin/bash buildslave ;\
    echo "buildslave:$BUILDSLAVE_PASSWORD" | chpasswd ;\
    echo 'buildslave ALL = NOPASSWD: ALL' > /etc/sudoers.d/ci ;\
    chmod 0440 /etc/sudoers.d/ci ;\
    # Cleanup
    apt clean ;\
    rm -rf /var/lib/apt/lists/* /tmp/*

USER buildslave

RUN set -e ;\
    # Set git default config
    git config --global user.email "ci@morello-project.org" ;\
    git config --global user.name "Morello CI" ;\
    git config --global color.ui "auto"

WORKDIR /home/buildslave
CMD ["/bin/bash"]
